package ru.t1.aksenova.tm.service;

import ru.t1.aksenova.tm.api.ICommandRepository;
import ru.t1.aksenova.tm.api.ICommandService;
import ru.t1.aksenova.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
